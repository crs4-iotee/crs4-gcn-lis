from PIL import Image
import numpy as np
import cv2
import matplotlib.pyplot as plt
from copy import deepcopy

cap=cv2.VideoCapture('signer9_sample45_color.mp4')
ret, frame = cap.read()
frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
ermy2 = np.load('test_data_joint.npy')
print(ermy2.shape)
pred_ermy2 = ermy2[0,:,0,:,0].transpose()
pred_ermy2.shape
img = deepcopy(frame)
for j in range(27):
    img = cv2.circle(img,(512-int(pred_ermy2[j,0]), int(pred_ermy2[j,1])), radius=5, color=(0,255,0),thickness=-1)
plt.imshow(img)
plt.show()

#if __name__ == '__main__':
#    main()

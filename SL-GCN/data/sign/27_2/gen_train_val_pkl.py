'''
import numpy as np


data_train = np.load('train_label.pkl')
data_val = np.load('val_label.pkl')

data_train_val = np.concatenate((data_train, data_val), axis=0)
#    print(data_train_val.shape)
np.save(('train_val_label.pkl'), data_train_val)
'''

import pickle

my_dict_final = []  # Create an empty dictionary
with open('train_label.pkl', 'rb') as f:
	trl = pickle.load(f)
	names_tr = trl[0]
	vals_tr = trl[1]

with open('val_label.pkl', 'rb') as f:
	val = pickle.load(f)
	names_val = val[0]
	vals_val = val[1]

final_names = names_tr + names_val
final_vals = vals_tr + vals_val

with open("train_val_labels.pkl", 'wb') as f:
	pickle.dump((final_names, final_vals), f)

with open('train_val_labels.pkl', 'rb') as f:
	tr_val = pickle.load(f)
	print(tr_val)
#!/bin/sh
python data_process/wholepose_skeleton/demo_LIS.py
mv data_process/wholepose_skeleton/npy_tmp/signer27_sample1_color.mp4.npy data_process/wholepose_skeleton/npy_webcam_LIS/.

python SL-GCN/data_gen_lis/sign_gendata.py
cp SL-GCN/data/sign/27/test_data_joint.npy SL-GCN/data/sign/27_2/.
cp SL-GCN/data/sign/27/test_label.pkl SL-GCN/data/sign/27_2/.
python SL-GCN/data_gen_lis/gen_bone_data.py
python SL-GCN/data_gen_lis/gen_motion_data.py

python SL-GCN/main_ermy.py --config ./SL-GCN/config/sign/test/test_joint.yaml --device=0
python SL-GCN/main_ermy.py --config ./SL-GCN/config/sign/test/test_bone.yaml --device=0
python SL-GCN/main_ermy.py --config ./SL-GCN/config/sign/test/test_joint_motion.yaml --device=0
python SL-GCN/main_ermy.py --config ./SL-GCN/config/sign/test/test_bone_motion.yaml --device=0

cp SL-GCN/work_dir/joint_27_2_test/eval_results/best_acc.pkl ensemble/gcn/joint.pkl
cp SL-GCN/work_dir/bone_27_2_test/eval_results/best_acc.pkl ensemble/gcn/bone.pkl
cp SL-GCN/work_dir/joint_motion_27_2_test/eval_results/best_acc.pkl ensemble/gcn/joint_motion.pkl
cp SL-GCN/work_dir/bone_motion_27_2_test/eval_results/best_acc.pkl ensemble/gcn/bone_motion.pkl


cp SL-GCN/data/sign/27/test_label.pkl ensemble/gcn/.
python ensemble/gcn/ensemble_wo_val.py

from sys import stdout
from makeup_artist import Makeup_artist
import logging
from flask import Flask, render_template, Response
from flask_socketio import SocketIO, emit
from camera import Camera
from utils import base64_to_pil_image, pil_image_to_base64
import cv2
import numpy as np
import base64
import io, os
from imageio import imread
import matplotlib.pyplot as plt
import time
import csv

app = Flask(__name__)
app.logger.addHandler(logging.StreamHandler(stdout))
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True
socketio = SocketIO(app)
camera = Camera(Makeup_artist())
start_record = False
FRAMES_COUNT = 0
N_FRAMES = 60
fps = 20.0
out = None

ROOT_PATH ="/home/iot01/cvpr21chal-slr-ermy/LIS_sardegna_WEBCAM"
FILE_PREDICTIONS = "predictions_wo_val.csv"

@socketio.on('input image', namespace='/test')
def test_message(input):
    global out
    global start_record
    global FRAMES_COUNT   
    global N_FRAMES
    global fps
    global ROOT_PATH
    
    input = input.split(",")[1]
    camera.enqueue_input(input)
    image_data = input # Do your magical Image processing here!!
    #image_data = image_data.decode("utf-8")
    img = imread(io.BytesIO(base64.b64decode(image_data)))
    
    if start_record == True:
            if start_record == True and FRAMES_COUNT == 0:
                out = cv2.VideoWriter(ROOT_PATH + "/data/webcam_mp4/signer27_sample1_color.mp4", cv2.VideoWriter_fourcc(*'mp4v'), fps, (512, 512), True)
                print("video container created")
            if FRAMES_COUNT < N_FRAMES:
                frame_r = cv2.resize(img, (512, 512), interpolation=cv2.INTER_AREA)
                print("write frame n: " + str(FRAMES_COUNT))
                out.write(frame_r)
                FRAMES_COUNT = FRAMES_COUNT + 1

            elif FRAMES_COUNT == N_FRAMES:
                out.release()
                FRAMES_COUNT = 0
                start_record = False


@socketio.on('connect', namespace='/test')
def test_connect():
    app.logger.info("client connected")


def runcmd(cmd, verbose = False, *args, **kwargs):

    process = subprocess.Popen(
        cmd,
        stdout = subprocess.PIPE,
        stderr = subprocess.PIPE,
        text = True,
        shell = True
    )
    std_out, std_err = process.communicate()
    if verbose:
        print(std_out.strip(), std_err)
    pass



def extract_word():
    global FILE_PREDICTIONS
    row1 = None
    with open(FILE_PREDICTIONS, newline='') as f:
        reader = csv.reader(f)
        row1 = next(reader)  # gets the first line
    
    with open("Progetto IPOACUSIA_Vocabolario PA_120 segni definitivo.csv") as file_name:
        file_read = csv.reader(file_name)
        array = list(file_read)
    
    return array[int(row1[1])-1][0].split(" ")[1]

@app.route('/start_detection')
def video_detection():
    global FILE_PREDICTIONS
    print("Video detection started!!")
    runcmd("rm -rf " + FILE_PREDICTIONS)
    runcmd("./script.sh", verbose=True)
    word = "Predizioni non generate."
    status = "fail"
    if os.path.exists(FILE_PREDICTIONS):
        print("predizioni generate nel file " +FILE_PREDICTIONS)
        word = extract_word()
        status = "success"
    else:
        print("predizioni non generate.")    
    return {"status": status, "response": word}



@app.route('/start_record')
def _start_record():
    global start_record
    print("start_record: {}".format(start_record))
    start_record=True
    print("start_record: {}".format(start_record))
    
    while start_record:
        if not start_record:
            break
        time.sleep(2)
    return {"msg": "success"}

@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')


def gen():
    """Video streaming generator function."""

    app.logger.info("starting to generate frames!")
    while True:
        frame = camera.get_frame() #pil_image_to_base64(camera.get_frame())

        print(type(frame))
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


# @app.route('/video_feed')
# def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    # return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port="5000")

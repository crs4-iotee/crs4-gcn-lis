# CRS4-GCN-LIS

This repository contains the reduced version of the extended code
available at
https://github.com/jackyjsy/CVPR21Chal-SLR. In particular, the skeleton
modality,SL-GCN, is given. This approach and model has been chosen to
recognise the signs of the italian sign language (LIS), for wich the new
dataset CRS4-LIS has been
created. Such dataset is composed of 120 signs and 27 signers, with all
videos
RGB modality and half of them also in Infrared, Depth and Skeleton
modality.

# Install and run

To test the solution please make follow instructions

A ```requirements.txt ``` file for installation is provided. The project is developed in  ``` conda ``` evironment.

```
git clone https://gitlab.com/crs4-iotee/crs4-gcn-lis.git
cd crs4-gcn-lis
python app.py
```

then point your prefered browser to 

```
http://localhost:5000
```
